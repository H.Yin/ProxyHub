from django.apps import AppConfig


class ToolkitConfig(AppConfig):
    name = 'ToolKit'
