from django.db import models

# Create your models here.


class ProxyItem(models.Model):
    '''
    store proxy
    '''

    HTTP_TYPE = (
        (1, 'HTTP'),
        (2, 'HTTPS'),
    )

    ip = models.CharField('IP Address', max_length=30)
    port = models.IntegerField('Port')
    http_type = models.IntegerField('Http type', null=True, choices=HTTP_TYPE)
    connect_time = models.DecimalField('Connect time', max_digits=10, decimal_places=6)
    # desc order by alive_time
    alive_time = models.IntegerField('Alive time', null=False, default=0)
    try_success = models.IntegerField('Try success times', default=0)
    try_failed = models.IntegerField('Try failed times', default=0)
    update_time = models.DateTimeField('Update Time', auto_now=True)

    def __unicode__(self):
        return "%s://%s:%s %s" % (
            self.get_http_type_display(),
            ip, str(port), str(connect_time)
        )

    class Meta:
        verbose_name_plural = 'Proxy item'
        db_table = 't_proxy_item'
