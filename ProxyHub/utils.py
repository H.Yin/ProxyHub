
import logging
import requests
from requests.exceptions import ProxyError, ConnectTimeout
from ToolKit.useragent import get_user_agent


LOG = logging.getLogger(__name__)


HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,pl;q=0.7,zh-TW;q=0.6,fr;q=0.5,de;q=0.4",
    "Cache-Control": "no-cache",
    "Connection": "close",
    "Host": "",
    "Pragma": "no-cache",
    "User-Agent": "",
}


def get_proxy():

    proxies = ProxyItem.objects.all()
    for proxy in proxies:
        status = proxy_check_status(
            proxy.ip,
            proxy.port,
            proxy.http_type
        )


def get_html(url, encode=False):
    ret = b''
    try:
        HEADERS['host'] = 'www.xicidaili.com'
        HEADERS['User-Agent'] = get_user_agent()
        res = requests.get(url, headers=HEADERS)
        if res.status_code == 200:
            ret = res.content
        else:
            LOG.error("%d:%s" % (res.status_code, res.content))
    except Exception as _ex:
        LOG.error(str(_ex))
    return ret


def check_proxy_status(ip, port, http_type='http'):
    '''
    check whether proxy is alive or not.
    '''
    status = {
        'valid': False,
        'connect_time': 0.0
    }
    proxies = {}
    proxies[http_type] = "%s://%s:%s" % (http_type, ip, str(port))
    try:
        res = requests.get("http://www.baidu.com", proxies=proxies, timeout=10)
        LOG.info("%s:%s" % (proxies[http_type], str(res.status_code)))
        if res.status_code == 200:
            status['valid'] = True
            status['connect_time'] = res.elapsed.total_seconds()
        else:
            status['valid'] = False
    except Exception as _ex:
        LOG.error(proxies[http_type])
    return status


def convert_time(time_str=''):
    '''
    convert time with unit to minutes
    '''
    UNIT = {
        '天': 24 * 60,
        '小时': 60,
        '分钟': 1,
    }
    try:
        for (name, unit) in UNIT.items():
            if name in time_str:
                return int(time_str.rstrip(name)) * unit
    except ValueError as _ex:
        LOG.error('convert_time: %s-> %s' % (time_str, str(_ex)))

    return 0
