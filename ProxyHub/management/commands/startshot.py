

import logging
from multiprocessing import Process
from lxml import etree

from django.core.management.base import BaseCommand, CommandError

from ProxyHub.models import ProxyItem
from ProxyHub.utils import *
from ProxyHub.settings import TARGET, FILTER

log = logging.getLogger(__name__)


class Command(BaseCommand):
    '''

    '''

    def _parser(self, html):
        proxies = []
        tree = etree.HTML(html)
        records = tree.xpath(TARGET['xpath']['record'])
        for item in records:
            temp = [td.strip() for td in item.itertext(with_tail=False)]
            proxy = {}
            proxy['ip'] = temp[1]
            proxy['port'] = int(temp[2])
            proxy['http_type'] = temp[6].lower()
            proxy['alive_time'] = convert_time(temp[14])
            LOG.info(proxy)
            proxies.append(proxy)
        
        return proxies

    def worker(self, number):
        '''
        get -> parse -> store
        '''
        url = TARGET['url_base'] + (TARGET['url_ext'] % number)
        LOG.info(url)
        html = get_html(url)
        if not html:
            return
        proxies = self._parser(html)
        print(proxies)
        http_type_dict = {item[1]: item[0] for item in ProxyItem.HTTP_TYPE}
        for p in proxies:
            status = check_proxy_status(p['ip'], p['port'], p['http_type'])
            if status['valid']:
                ProxyItem(ip=p['ip'], port=p['port'], 
                        http_type=http_type_dict.get(p['http_type']),
                        connect_time=status['connect_time'],
                        alive_time=p['alive_time'], try_success=1,
                    ).save()

    def handle(self, *args, **options):
        try:
            p = Process(target=self.worker, args=(1,))
            p.start()
            p.join()
        except Exception as _ex:
            print(str(_ex))
