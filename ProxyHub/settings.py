
TARGET = {
    'host': 'www.xicidaili.com',
    'url_base': 'http://www.xicidaili.com/nn/',
    'url_ext': '%d',
    'xpath': {
        'record': '//table[@id="ip_list"]/tr[position()>1]',
    }
}

FILTER = {
    'http_type': 'HTTPS'
}
