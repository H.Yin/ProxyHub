from django.apps import AppConfig


class ProxyhubConfig(AppConfig):
    name = 'ProxyHub'
